import sys
import operator
#this is just a simple online frequency checker for text in 4k chunks
#but it is the basis of a lz algorithm approximator lza
table = {}
def show_status():
	sorted_table = sorted(table.iteritems(), key=operator.itemgetter(1))
        for word in sorted_table:
		print word[0] + ''.join([' ' for i in xrange(40-len(word[0]))]) + `word[1]`
	raw_input("Dance the Macarena to continue")

def lz(s):
	words = s.split()
	for word in words:
		if word in table:
			table[word] += 1
		else:
			table[word] = 1
        show_status()
	

with open(sys.argv[1], 'r') as f:
        process = f.read(4096)
	while len(process) > 0:
	        lz(process)
		process = f.read(4096)
f.closed

show_status()


