import sys
import operator

#lz in python with simple ascending code and online progress report
gcount = 0
code = 0
def next_code():
	global code
	code += 1
	return code

class Trienode:
	childnodes = None
	data = None
	counts = 0
	code = None

	def __init__(self,idata):
		global gcount
		gcount += 1
		self.childnodes = {}
		self.data = idata
	
	def get(self,ichild,newness):
		if ichild not in self.childnodes:
			#print ""+ichild+ " not in "+ `self.data`
			newness = newness | 1
			self.childnodes[ichild] = Trienode(ichild)
		childnode = self.childnodes[ichild]
		childnode.counts += 1
		return childnode, newness
	
	def test(self,ichild):
		if ichild not in self.childnodes:
			return None
		return self.childnodes[ichild]

	def giveCode(self):
		self.code = next_code() 

	def hasCode(self):
		return self.code is not None

	def children(self):
		for ichild in self.childnodes:
			yield self.childnodes[ichild]

	def append(self,appendage):
		#changes state but also returns info on whether we changed state
		nextNode = self
		newness = 0
		for i in xrange(len(appendage)):
			nextNode,newness = nextNode.get(appendage[i],newness)
		return nextNode,newness

	def walk(self,path):
		#non destructive does not change state idempotent
		nextNode = self
		for i in xrange(len(appendage)):
			nextNode = nextNode.test(appendage[i])
			if nextNode is None:
				return None
		return nextNode

	def infoTuple(self):
		return self.counts, self.code 

class Trie:
        rootnode = None
	currnode = None

	def __init__(self):
		self.rootnode = Trienode(0)
		self.currnode = self.rootnode

        def addWord(self,aword):
		#non-idempotent (better syn?)
		self.currnode,newness = self.currnode.append(aword)
		if newness == 1:#equivalently if nextNode.code is None: ...
		    self.currnode.giveCode()
		    self.currnode,nn = self.rootnode.get(aword,0)
		return newness
	
	def containsWord(self,aword):
		#idempotent
		nextNode = self.rootnode.walk(aword)
		return nextNode

	def words(self):
		stack = [self.rootnode]
		path = ["\0"]
		while len(stack) > 0:
			nextNode = stack.pop()
			path.pop()
			for child in children(nextNode):
				stack.push(child)
				path.push(child.data)
				if child.hasCode():
					yield child, ''.join(path)
			
	def allwords(self):
		allwordsa = []
		for anode,aword in self.words():
			allwordsa.push((aword,anode.counts))
		return allwordsa

trie = Trie()
word = ''
lengthcount = 0
runningcount = 0

def make_initial_dict():
	for i in range(32,128):
         	trie.addWord(""+chr(i))
	trie.addWord("\n")
	trie.addWord("\r")
	trie.addWord("\t")

def collect_stats():
	global code
	runningcount = 0
	lengthcount = 0
	for anode,aword in trie:
	         codefreq = anode.counts 
		 runningcount += codefreq
		 lengthcount += codefreq*len(aword)

def show_status():
	global code,lengthcount,runningcount
	print ("\nAverage code length: %.2f" % (lengthcount*1.0/runningcount))
	print "\nCode up to:" +`code`
	a = "" #raw_input("\nEnter continues, anything else then enter displays all codes")
	if len(a) > 0:
	        sorted_table = sorted(trie.allwords(), key=lambda(k,v):v)
		for aword in sorted_table:
	            print aword 
	        raw_input("\nDance the Macarena to continue")

#should shift to HTML output with hover popup for clarity
def show_code_h(aword):
	sys.stdout.write(aword + "(" + ','.join(map(str,trie.containsWord(aword).infoTuple())) + ")")

#just showing segmentation
def show_code(aword):
	if "\r" in aword:
	    aword = aword.replace("\r","\n\r")
	sys.stdout.write(''.join(["-" , aword]))

def lz(s):
	global trie, word, gcount,code,lengthcount,runningcount
	for i in xrange(len(s)):
		nextchar = s[i]
		nextword = ''.join([word,nextchar])
		intrie = True if trie.addWord(nextchar) == 0 else False
		#this is O(s)
		#we can speed this up by just checking if the nextchar launches off from the previous final state
		#an optimization that makes addword O(1)
		if intrie:
			#keep building our word if the prefix is already in the trie
			word = nextword
		else:
			#otherwise start again if we just build a new word that is not a prefix of anything yet
		        #show_code(word)
			word = nextchar
		#print gcount
		lengthcount += len(nextword)
		runningcount += 1
	show_status()
	
make_initial_dict()
with open(sys.argv[1], 'r') as f:
        process = f.read(4096)
	while len(process) > 0:
	        lz(process)
		process = f.read(4096)
f.closed

show_status()


